---
title: Navigation Bar
subtitle: Как скрыть NavigationBar только на первом контроллере
date: 2018-06-12
#bigimg: [{src: "/img/path.jpg", desc: "Path"}]
tags: ["ios", "swift"]
---
<!--more-->

Используется несколько вариантом подстветки кода:

Подсветка кода с использование разметки markdown `swift ... `
```swift
override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    // Hide the navigation bar on the this view controller
    self.navigationController?.setNavigationBarHidden(true, animated: animated)
}

override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    // Show the navigation bar on other view controllers
    self.navigationController?.setNavigationBarHidden(false, animated: animated)
}
```

{{< highlight swift "linenos=inline">}}
override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    // Hide the navigation bar on the this view controller
    self.navigationController?.setNavigationBarHidden(true, animated: animated)
}

override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    // Show the navigation bar on other view controllers
    self.navigationController?.setNavigationBarHidden(false, animated: animated)
}
{{</ highlight >}}